package com.zielware.Shogun;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShogunApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShogunApplication.class, args);
	}
}
